package utility

import (
	"github.com/hashicorp/consul/api"
	log "github.com/sirupsen/logrus"
)

func RegisterService(consulhost *string, host string, serviceName string, port int) {
	defualtConf := api.DefaultConfig()
	defualtConf.Address = *consulhost
	client, err := api.NewClient(defualtConf)
	if err != nil {
		panic(err)
	}

	serviceItem := ParepareRegSer(host, serviceName, serviceName, port)
	err = client.Agent().ServiceRegister(serviceItem)
	if err != nil {
		log.Error(err.Error())
		return
	}
}
