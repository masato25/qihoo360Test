package utility

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/hashicorp/consul/api"
	"github.com/masato25/qihoo360Test/consul/consul_app/g"
)

func ParepareReg(ip string, name string, id string, port int) (reg *api.CatalogRegistration) {
	conf := g.Config()
	services := &api.AgentService{
		ID:      fmt.Sprintf("%s-%s", name, id),
		Service: name,
		Tags:    []string{name},
		Port:    port,
		Address: ip,
	}

	reg = &api.CatalogRegistration{
		Datacenter: conf.DATACENTER,
		Address:    selectAvailableConsulServer(conf.ADDRESSES, conf.HTTPPORT),
		Node:       conf.NODE,
		Service:    services,
	}
	return
}

func ParepareRegSer(ip string, name string, id string, port int) (reg *api.AgentServiceRegistration) {
	conf := g.Config()
	check := &api.AgentServiceCheck{
		TCP:      fmt.Sprintf("%s:%d", ip, port),
		Status:   api.HealthPassing,
		Interval: "20s",
		Timeout:  "3s",
	}
	reg = &api.AgentServiceRegistration{
		Address:           selectAvailableConsulServer(conf.ADDRESSES, conf.HTTPPORT),
		Check:             check,
		ID:                fmt.Sprintf("%s-%s", name, id),
		Name:              name,
		Tags:              []string{name},
		Port:              port,
		EnableTagOverride: false,
	}
	return
}

func selectAvailableConsulServer(consult []string, port int) string {
	address := "0.0.0.0"
	for _, addr := range consult {
		_, err := CheckConsulServer(addr, port)
		if err == nil || strings.Contains(err.Error(), "malformed HTTP response") {
			address = addr
			return address
		} else {
			log.Println("consul server check: ", err.Error())
		}
	}
	// return address
	return "8.8.8.8"
}

func CheckConsulServer(host string, port int) (*http.Response, error) {
	timeout := time.Duration(3 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	url := fmt.Sprintf("http://%s:%d", host, port)
	resp, err := client.Get(url)
	return resp, err
}
