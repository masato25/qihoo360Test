package computing_servies

import (
	"strings"
)

func UpperMapValues(data map[string]string) (output map[string]string) {
	output = make(map[string]string, len(data))
	for k, v := range data {
		output[k] = strings.ToUpper(v)
	}
	return
}

/* ***
0 means select max weight
others means select min weigh
*** */
func SelectWeight(data map[string]int, selectType int) string {
	if selectType == 0 {
		return SelectMax(data)
	} else {
		return SelectMin(data)
	}
}

type SelectedObject struct {
	Key    string
	Weight int
}

func SelectMax(data map[string]int) string {
	fristKey := GetFristKeyFromMap(data)
	result := SelectedObject{fristKey, data[fristKey]}
	for k, v := range data {
		if result.Weight <= v {
			result.Key = k
			result.Weight = v
		}
	}
	return result.Key
}

func SelectMin(data map[string]int) string {
	fristKey := GetFristKeyFromMap(data)
	result := SelectedObject{fristKey, data[fristKey]}
	for k, v := range data {
		if result.Weight >= v {
			result.Key = k
			result.Weight = v
		}
	}
	return result.Key
}

func GetFristKeyFromMap(data map[string]int) string {
	var key string
	for k, _ := range data {
		key = k
		break
	}
	return key
}
