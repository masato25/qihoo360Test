package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/masato25/qihoo360Test/app/controller/computing_servies"
)

func Routes(route *gin.Engine) {
	computing_servies.Route(route)
}
