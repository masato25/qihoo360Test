package test

import (
	"fmt"
	"testing"

	"github.com/masato25/qihoo360Test/benchmark_tool/utility"
	. "github.com/smartystreets/goconvey/convey"
)

var duration int = 10 //seconds
var goroutines int = 2
var timeoutms int = 1000 // request timeout in ms
var allowRedirectsFlag bool = false
var disableCompression bool = false
var disableKeepAlive bool = false
var http2 bool = true
var clientCert string
var clientKey string
var caCert string
var domain string = "localhost:8088"

func TestUpper(t *testing.T) {
	Convey("Test Benchmark for uppder services", t, func() {
		utility.CallHttpRequest("localhost", fmt.Sprintf("http://%s/upper", domain), "POST", "{\"bar\": \"A@FB\", \"foo\": \"AB!C\"}")
	})
}

func TestSelect(t *testing.T) {
	Convey("Test Benchmark for select services", t, func() {
		utility.CallHttpRequest("localhost", fmt.Sprintf("http://%s/select", domain), "POST", "{\"Apple\": 2,\"Foo\": 3}")
	})
}
